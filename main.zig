const std = @import("std");
const print = std.debug.print;

const Picture = @import("Picture.zig").Picture;
const Allocator = std.mem.Allocator;
const Colors = @import("Picture.zig").Colors;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    defer {
        const leaked = gpa.deinit();
        if (leaked) @panic("leaked");
    }

    var pic = try Picture.init(allocator, 800, 600);
    defer pic.deinit();

    pic.fill(Colors.WHITE);

    try pic.setPixel(Colors.RED, 7, 5);

    try pic.drawRectangle(Colors.GREEN, 150, 150, 400, 200);

    try pic.drawRectangle(Colors.BLUE, 300, 300, 100, 200);

    try pic.saveAsPpm("px.ppm");
}
