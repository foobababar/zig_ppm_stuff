const std = @import("std");
const print = std.debug.print;
const Allocator = std.mem.Allocator;
const File = std.fs.File;


pub const Colors = struct {
  pub const BLACK = 0x000000;
  pub const WHITE = 0xFFFFFF;
  pub const RED = 0xFF0000;
  pub const GREEN = 0x00FF00;
  pub const BLUE = 0x0000FF;
};

const Error = error {
  ValueTooBig,
};


pub const Picture = struct {
  WIDTH: u32,
  HEIGHT: u32,
  picture :[]u24,
  allocator: Allocator,

  pub fn init(allocator: Allocator, w: u32, h: u32) !Picture {
    return Picture {
      .WIDTH = w,
      .HEIGHT = h,
      .picture = try allocator.alloc(u24, w*h), 
      .allocator = allocator,
    };
  }

  pub fn deinit(self: *Picture) void {
    self.allocator.free(self.picture);
  }

  pub fn fill(self: *Picture, color: u24) void {
    var i: u32 = 0;
    while(i<self.picture.len):(i+=1){
      self.picture[i]=color;
    }
  }

  pub fn setPixel(self: *Picture, color: u24, x: u32, y: u32) !void {
    //self.WIDTH and self.HEIGHT not known @ comptime ?

    // throw or don't throw error ?
    if(x>=self.WIDTH or y>=self.HEIGHT){
      return Error.ValueTooBig;
    }
    self.picture[self.WIDTH*y+x]=color; 
  }


  //draw rectangles
  pub fn drawRectangle(self: *Picture, color: u24, x: u32, y: u32, width: u32, height: u32) !void {
    var i: u32 = x;
    while(i<x+width):(i+=1){
      var j: u32 = y;
      while(j<y+height):(j+=1){
        try self.setPixel(color, i, j);    
      }
    }
  }


  pub fn saveAsPpm(self: *Picture, filename: []const u8) !void {
    const file = try std.fs.cwd().createFile(filename, .{.read=true},);
    defer file.close();

    //format and save header into file
    try self.writeBytesToFile(file, &[_]u8{80, 54, 10});//P6 header 

    //temporary buffer to store formated slice
    var buffer: [50]u8 = undefined; 

    //width
    for( std.fmt.bufPrintIntToSlice(buffer[0..], @intCast(i32, self.WIDTH), 10, .upper, std.fmt.FormatOptions{}) )|byte|{
      //transform width into bytes. For example : 
      //                            * 10 -> 1 0 (2 bytes)
      //                            * 800 -> 8 0 0 (3 bytes)
      try self.writeByteToFile(file, byte );
    }
  
    //separator
    try self.writeByteToFile(file, 32);
  
    //height
    for( std.fmt.bufPrintIntToSlice(buffer[0..], @intCast(i32, self.HEIGHT), 10, .upper, std.fmt.FormatOptions{}) )|byte|{
      try self.writeByteToFile(file, byte );
    }
  
    //end of header
    try self.writeBytesToFile(file, &[_]u8{10, 50, 53, 53, 10});

    //write each pixel into file
    var i:u32 = 0;
    while(i<self.picture.len):(i+=1){
      // extract RGB values from u24 :
      // R value == color >> 16
      // G value == (color >> 8) & 0xFF
      // B value == color & 0xFF
      try self.writeBytesToFile(file, &[_]u8{ @intCast(u8, self.picture[i]>>16), @intCast(u8, (self.picture[i]>>8) & 0xFF), @intCast(u8, self.picture[i] & 0xFF) });
    }
  }


  pub fn writeByteToFile(self: *Picture, file: File,  byte : u8) !void {
    _ = self;
    try file.writer().writeByte(byte);
  }
  

  pub fn writeBytesToFile(self: *Picture, file: File, bytes: []const u8) !void {
    _ = self;
    for(bytes) |byte| {
      try file.writer().writeByte(byte); 
    }
  }

};
